var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    // Collapse all panels before expanding the clicked panel
    var panels = document.getElementsByClassName("panel");
    for (j = 0; j < panels.length; j++) {
      if (panels[j].style.maxHeight) {
        panels[j].style.maxHeight = null;
        panels[j].previousElementSibling.classList.remove("active");
        panels[j].previousElementSibling.className = panels[j].previousElementSibling.className.replace('icon-remove-circle','icon-ok-circle');  
      }
    }
    
    // Toggle the clicked panel
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
      this.className = this.className.replace('icon-ok-circle','icon-remove-circle');
    } 
  });
}